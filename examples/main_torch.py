from pool_resources import PoolResources
from torch_resource import TorchResource
import torch as tr

def f(x):
    return x**2

def main():
    resources = [TorchResource(tr.device("cpu:1")), TorchResource(tr.device("cuda"))]
    # resources = []
    pool = PoolResources(resources)
    seq = [tr.Tensor([1]), tr.Tensor([2]), tr.Tensor([3]), tr.Tensor([1, 2, 99])]
    res = pool.map(f, seq)
    print(res)

if __name__ == "__main__":
    main()