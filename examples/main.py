from pool_resources import PoolResources
from resource import DummyResource

def f(x):
    return x**2

def main():
    resources = [DummyResource(1), DummyResource(2)]
    pool = PoolResources(resources)
    seq = [1, 2, 3]
    res = pool.map(f, seq)
    print(res)

if __name__ == "__main__":
    main()