import os
import sys
import torch as tr
import numpy as np
from torch.utils.data import DataLoader, random_split
from torch.nn import functional as F
from torch import optim, nn, no_grad

from torchvision.datasets import MNIST
from pytorch_lightning import Trainer, LightningModule
from pytorch_lightning.loggers import TensorBoardLogger

from pool_resources import PoolResources
from pool_resources.resource import TorchResource

def collate_fn(x):
    rgb = tr.FloatTensor(np.array([np.array(item[0]) for item in x])) / 255
    labels = tr.LongTensor([item[1] for item in x])
    return rgb, labels

def get_loader(train: bool) -> MNIST:
    reader = MNIST(os.environ["MNIST_DATASET_PATH"], train=train, download=True)
    if train:
        n_train, n_validation = int(0.8 * len(reader)), len(reader) - int(0.8 * len(reader))
        readers = random_split(reader, lengths=[n_train, n_validation])
    else:
        readers = [reader]
    return [DataLoader(reader, collate_fn=collate_fn, batch_size=50, num_workers=0) for reader in readers]

class ModelFC(LightningModule):
    def __init__(self):
        super().__init__()

        self.fc1 = nn.Linear(28 * 28, 100)
        self.fc2 = nn.Linear(100, 100)
        self.fc3 = nn.Linear(100, 10)

    def forward(self, x):
        x = x.view(-1, 28 * 28)
        y1 = F.relu(self.fc1(x))
        y2 = F.relu(self.fc2(y1))
        y3 = self.fc3(y2)
        return y3

    def configure_optimizers(self):
        return optim.SGD(self.parameters(), lr=0.01)

    def training_step(self, batch, batch_idx):
        x, gt = batch
        y = self.forward(x)
        gt_one_hot = F.one_hot(gt, num_classes=10).type(tr.float)
        loss = F.cross_entropy(y, gt_one_hot).mean()
        return loss

    def validation_step(self, batch, batch_idx: int, *args, **kwargs):
        x, gt = batch
        with no_grad():
            y = self.forward(x)
        gt_one_hot = F.one_hot(gt, num_classes=10).type(tr.float)
        loss = F.cross_entropy(y, gt_one_hot).mean()
        return loss

def train_one(item):
    train_loader, validation_loader, model, version = item
    Trainer(max_epochs=10, logger=TensorBoardLogger(save_dir="experiments", version=version)) \
        .fit(model, train_loader, validation_loader)
    return model

def main():
    n = int(sys.argv[1])
    m = int(sys.argv[2])
    print(f"Training {m} networks on {n} devices")

    items = []
    for i in range(n):
        train_loader, validation_loader = get_loader(train=True)
        model = ModelFC()
        items.append([train_loader, validation_loader, model, i])
    
    if n == 0:
        _ = list(map(train_one, items))
    else:
        resources = [TorchResource(f"cpu:{i}") for i in range(n-1)]
        if tr.cuda.is_available():
            resources.append(TorchResource(f"cuda:0"))
        else:
            resources.append(TorchResource(f"cpu:{n-1}"))
        _ = PoolResources(resources, timeout=1).map(train_one, items)


if __name__ == "__main__":
    main()
